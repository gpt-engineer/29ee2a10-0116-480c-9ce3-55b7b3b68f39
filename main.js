document.getElementById('task-form').addEventListener('submit', function(event) {
  event.preventDefault();

  const taskInput = document.getElementById('task-input');
  const taskList = document.getElementById('task-list');

  const newTask = document.createElement('li');
  newTask.innerHTML = `
    <input type="checkbox" class="mr-2" />
    <span>${taskInput.value}</span>
    <button class="ml-auto bg-red-500 text-white p-1">
      <i class="fas fa-trash-alt"></i>
    </button>
  `;
  newTask.classList.add('flex', 'justify-between', 'items-center', 'p-2', 'bg-white', 'border-2', 'border-gray-300');

  newTask.querySelector('button').addEventListener('click', function() {
    taskList.removeChild(newTask);
  });

  newTask.querySelector('input[type="checkbox"]').addEventListener('change', function(event) {
    newTask.querySelector('span').style.textDecoration = event.target.checked ? 'line-through' : 'none';
  });

  taskList.appendChild(newTask);

  taskInput.value = '';
});
